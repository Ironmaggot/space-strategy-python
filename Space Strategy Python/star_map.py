import pygame, random, math
from star import Star, Planet
from population import Population
from polity import Polity
from species import Species
from misc_functions import choose_random_star_name, distance
from tooltip import tooltip
from action_menu import action_menu
from map_overlay import Map_Overlay

def generate_star_coordinates(gameworld_size_X, gameworld_size_Y, stars):
    valid_coordinate_found = False

    while valid_coordinate_found == False:
        X = random.randint(30, (gameworld_size_X - 30))
        Y = random.randint(30, (gameworld_size_Y - 30))

        if len(stars) != 0:
            valid_coordinate_found = True
            for star in stars:
                dist = distance((X, Y), star.coordinates)
                if dist < 50:
                    valid_coordinate_found = False
        else:
            valid_coordinate_found = True

    coordinates = (X, Y)
    return coordinates

class StarMap(object):
    """StarMap class is responsible for displaying and moving the starmap about"""

    def __init__(self, gameworld_size_X, gameworld_size_Y, amount_of_stars, resolution, font, max_planets):
        self.gameworld_size_X = gameworld_size_X
        self.gameworld_size_Y = gameworld_size_Y
        self.offset = [0, 0]
        self.resolution = resolution
        self.font_object = font
        self.week = 0
        self.pop_list = []
        self.req_pop_points = None
        self.planet_list = []
        self.click_event = False

        self.polity_list = []
        self.civilization_amount = 10
        self.starting_pops = 10
        self.specie_list = []

        self.selected = None #Can be either a star or a polity

        self.stars = []
        for i in range(amount_of_stars):
            #Create the star
            star_coordinates = generate_star_coordinates(gameworld_size_X, gameworld_size_Y, self.stars)
            name = choose_random_star_name()

            star = Star(star_coordinates, i, name)

            #Generate planets for the star
            amount_of_planets = random.randint(1, max_planets)
            for planet_id in range(amount_of_planets):
                planet = Planet()
                planet.size = random.randint(1, 10)
                planet.owner = star
                planet.index = planet_id
                planet.gravity = random.randint(1, 3)
                self.planet_list.append(planet)
                star.planets.append(planet)
                
            self.stars.append(star)

        #generate civilizations
        for civilization in range(self.civilization_amount):
            polity = Polity()
            polity.name = choose_random_star_name()

            specie = Species()
            specie.name = polity.name
            self.specie_list.append(specie)
            potential_planets = []
            for planet in self.planet_list:
                if planet.gravity == specie.preferred_gravity:
                    if planet.owner.owner == None:
                        potential_planets.append(planet)

            amount_of_potential_planets = len(potential_planets)
            random_potential_planet_index = (random.randint(1, amount_of_potential_planets) - 1)
            random_potential_planet = potential_planets[random_potential_planet_index]

            for i in range(self.starting_pops):
                pop = Population(random_potential_planet, specie)
                random_potential_planet.pop_list.append(pop)
                self.pop_list.append(pop)

            random_star = random_potential_planet.owner
            random_star.name = polity.name

            polity.owned_stars.append(random_star)
            random_star.owner = polity

            self.polity_list.append(polity)
        
        self.polity_list[0].player = True
        self.polity_list[0].is_selected = True
        starting_offset_X = self.polity_list[0].owned_stars[0].coordinates[0] * -1
        starting_offset_Y = self.polity_list[0].owned_stars[0].coordinates[1] * -1
        self.offset = [int(starting_offset_X + (self.resolution[0] / 2)), int(starting_offset_Y + (self.resolution[1] / 2))]

        self.overlay = Map_Overlay(self)

    def colonize_star(self, origin_star, target_star): # Colonizes target star with a pop from origin star
        # 1.Choose a random pop from origin star
        pop_list = []
        for planet in origin_star.planets:
            for pop in planet.pop_list:
                pop_list.append(pop)

        if pop_list != []:
            chosen_pop = random.choice(pop_list)
        else:
            return # If there are no pops, then something has gone wrong somewhere, so do nothing

        # 2.Choose the best planet for the pop in the target star
        best_planet = None
        for planet in target_star.planets:
            if best_planet == None:
                best_planet = planet
            else:
                best_planet_desirability = best_planet.find_desirability(chosen_pop)
                pending_planet_desirability = planet.find_desirability(chosen_pop)
                if best_planet_desirability < pending_planet_desirability:
                    best_planet = planet

        # 3. Migrate the pop to the target star
        chosen_pop.migrate(best_planet)

        # 4. Create a polity over there
        new_polity = Polity()
        new_polity.name = target_star.name
        new_polity.owned_stars.append(target_star)
        best_planet.owner.owner = new_polity
        self.polity_list.append(new_polity)

        # 5. Deduct colonization cost from origin star owner
        cost = self.overlay.find_colonization_cost(origin_star, target_star)
        origin_polity = origin_star.owner
        origin_polity.money = origin_polity.money - cost

    def update(self, surface, tick):
        mouse_pos = pygame.mouse.get_pos()
        mouse_click = pygame.mouse.get_pressed()
        adjusted_mouse_X = mouse_pos[0] - self.offset[0]
        adjusted_mouse_Y = mouse_pos[1] - self.offset[1]
        adjusted_mouse_pos = (adjusted_mouse_X, adjusted_mouse_Y)

        #----This block is resbonsible for scrolling the map left and right, up and down
        if mouse_pos[0] < 10:
            self.offset[0] = self.offset[0] + 10
        elif mouse_pos[0] > (self.resolution[0] - 10):
            self.offset[0] = self.offset[0] - 10

        if mouse_pos[1] < 10:
            self.offset[1] = self.offset[1] + 10
        elif mouse_pos[1] > (self.resolution[1] - 10):
            self.offset[1] = self.offset[1] - 10

        if self.offset[0] > 500:
            self.offset[0] = 500
        elif self.offset[0] < ((self.gameworld_size_X * -1) + 1000):
            self.offset[0] = (self.gameworld_size_X * -1) + 1000

        if self.offset[1] > 500:
            self.offset[1] = 500
        elif self.offset[1] < (self.gameworld_size_Y * -1) + 500:
            self.offset[1] = (self.gameworld_size_Y * -1) + 500

        self.overlay.update(surface, adjusted_mouse_pos)

        for star in self.stars:
            star.update(self.offset)

        #----DATE and Stuff
        if tick == True:
            self.week = self.week + 1
        pygame.draw.rect(surface, (100, 100, 100), (10, 10, 400, 25))

        # Find player money
        player_polity = None
        for polity in self.polity_list:
            if polity.player == True:
                player_polity = polity
        player_money = str(int(player_polity.money))

        text_surf = str(self.week) + " (" + str(len(self.pop_list)) + ")" + " Player Money: " + player_money

        date_text = self.font_object.render(text_surf, 0, (200, 200, 200))
        surface.blit(date_text, (15, 10))

        #----Population
        if tick == True:
            for pop in self.pop_list:
                pop.update()
                if pop.pop_growth_points >= self.req_pop_points:
                    old_pop_loc = pop.location
                    new_pop = Population(old_pop_loc, pop.specie)
                    old_pop_loc.pop_list.append(new_pop)
                    self.pop_list.append(new_pop)
                    pop.pop_growth_points = random.randint(0, (self.req_pop_points / 10))
                elif pop.pop_growth_points < 0:
                    pop.location.pop_list.remove(pop)
                    self.pop_list.remove(pop)
                    del pop
                    continue

                if pop.migration_roll() == True:
                    destinations = pop.migration_destinations(self.planet_list)
                    amount_of_destinations = len(destinations)

                    if amount_of_destinations > 0:
                        random_element_index = random.randint(1, amount_of_destinations)
                        random_element_index = random_element_index - 1

                        destination_planet = destinations[random_element_index]

                        pop.migrate(destination_planet)

                        if destination_planet.owner.owner == None: # If a pop migrates to an uncolonized star system, a new polity is created
                            new_polity = Polity()
                            new_polity.name = destination_planet.owner.name
                            new_polity.owned_stars.append(destination_planet.owner)
                            destination_planet.owner.owner = new_polity
                            self.polity_list.append(new_polity)

            for polity in self.polity_list:
                polity.update()

            self.click_event = False
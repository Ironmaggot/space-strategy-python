import random
from star import Star

def random_color():
    R = random.randint(0, 255)
    G = random.randint(0, 255)
    B = random.randint(0, 255)
    color = (R, G, B)

    return color

class Polity(object):
    """Code for political entities is in here"""

    def __init__(self):
        self.color = random_color()
        self.name = None
        self.owned_stars = []
        self.money = 0
        self.tax_rate = 0.1
        self.player = False
        self.is_selected = False

    def update(self):
        # Collect taxes from population
        tax_collected = 0

        for star in self.owned_stars:
            for planet in star.planets:
                pop_amount = len(planet.pop_list)
                tax_collected = tax_collected + (self.tax_rate * pop_amount)

        self.money = self.money + tax_collected
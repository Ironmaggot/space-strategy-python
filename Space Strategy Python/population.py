import random, math

class Population(object):
    """This class handles the population"""

    def __init__(self, location, specie):
        self.location = location #has to be a planet to work
        self.pop_growth_points = 0
        self.specie = specie

    def update(self):
        habitat_modifier = (self.location.find_desirability(self) / 10)
        recieve_pop_points = self.specie.time_to_reproduce  + habitat_modifier
        self.pop_growth_points = self.pop_growth_points + recieve_pop_points

    def migration_roll(self): # See if pop wants to migrate
        migrate = False
        migration_roll_size = 1000
        migration_desire = self.location.find_base_desirability(self) - self.location.find_desirability(self)

        migration_roll_size = int(1000 / migration_desire)
        if migration_roll_size < 1:
            migration_roll_size = 1

        migration_roll = random.randint(1, migration_roll_size)
        
        if migration_roll == 1:
            migrate = True

        return migrate

    def migration_destinations(self, planet_list): #chooses where the pop would want to migrate to
        destinations = []
        home_desirability = self.location.find_desirability(self) + 1000 # Remove the '1000'
        
        for planet in planet_list:
            if (planet.owner.owner == None) or (planet.owner.owner == self.location.owner.owner):
                destination_desirability = planet.find_desirability(self)

                #following finds distance modifier

                home_coordinates = self.location.owner.coordinates
                destination_coordinates = planet.owner.coordinates

                delta_X = destination_coordinates[0] - home_coordinates[0]
                delta_Y = destination_coordinates[1] - home_coordinates[1]

                distance = math.hypot(delta_X, delta_Y)
                distance_modifier = distance / 10

                #---------
            
                destination_desirability = destination_desirability - distance_modifier
            
                if destination_desirability > home_desirability:
                    destinations.append(planet)

        return destinations

    def migrate(self, destination): #migrates pop to selected planet

        self.location.pop_list.remove(self)
        destination.pop_list.append(self)
        self.location = destination
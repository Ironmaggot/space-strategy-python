import pygame
from tooltip import tooltip
from misc_functions import distance
from action_menu import action_menu

class Map_Overlay(object):
    """Shows the star map and acts as the base level of UI"""

    def __init__(self, star_map):
        self.star_map = star_map
        self.map_tooltip = tooltip()

        self.focus = None # Has to be a star. At each frame the focus star is updated

        self.map_action_menu = action_menu(self)

    def get_player_polity(self):
        for polity in self.star_map.polity_list:
            if polity.player == True:
                return polity

    def colonize(self, origin_star, target_star):
        self.star_map.colonize_star(origin_star, target_star)

    def mouse_click(self, adjusted_mouse_pos):
        # This function determins where mouse was clicked and what is gonna be done about it
        #
        # If click is found somewhere, resolve it and then call "return" so that the function won't check against anything else
        #
        # It first goes through all the menus in priority order before looking at the map
        #
        # First click usually should mean that the polity is selected
        # If polity is already selected, then the star of that polity is selected
        # Uncolonized stars automatically select the star

        if self.star_map.click_event == True:
            self.star_map.click_event = False

            click = pygame.mouse.get_pressed()
            clicked_star = None

            # Find which star exactly was clicked on with left click if any
            if click[0] == 1:
                # Seeing if anything in menus was pressed
                if self.map_action_menu.is_active == True:
                    clicked = self.map_action_menu.mouse_click(self.star_map.offset)
                    if clicked == True:
                        return
                # Seeing what on the map was pressed
                for star in self.star_map.stars:
                    if self.map_action_menu.is_active == True:
                        if distance(self.map_action_menu.coordinates, star.coordinates) > self.map_action_menu.radius:
                            if distance(adjusted_mouse_pos, star.coordinates) < 20:
                                clicked_star = star
                                break
                    else:
                        if distance(adjusted_mouse_pos, star.coordinates) < 20:
                            clicked_star = star
                            break

                if clicked_star != None:
                    if clicked_star.owner == None: # If star is not a part of a polity, select it right away when clicked on
                        self.map_action_menu.selected_star = clicked_star
                        self.map_action_menu.coordinates = clicked_star.coordinates
                        self.map_action_menu.is_active = True

                        for polity in self.star_map.polity_list:
                            polity.is_selected = False
                    else:
                        selected_polity = None # See which polity is selected already
                        for polity in self.star_map.polity_list:
                            if polity.is_selected == True:
                                selected_polity = polity
                                break
                    
                        if selected_polity != None:
                            if selected_polity == clicked_star.owner: # See if that polity is the owner of that star
                                self.map_action_menu.selected_star = clicked_star
                                self.map_action_menu.coordinates = clicked_star.coordinates
                                self.map_action_menu.is_active = True
                            else:
                                selected_polity.is_selected = False # If the clicked star belongs to a different polity that is selected, select that polity instead
                                clicked_star.owner.is_selected = True
                                self.map_action_menu.is_active = False
                        else:
                            clicked_star.owner.is_selected = True
                            self.map_action_menu.is_active = False

            elif click[2] == 1: # If there is a right click, deselect star. If polity is selected, select player polity. If player polity is selected, do nothing.

                if self.map_action_menu.is_active == False:
                    for polity in self.star_map.polity_list:
                        if polity.player == True:
                            polity.is_selected = True
                        else:
                            polity.is_selected = False
                else:
                    self.map_action_menu.is_active = False

    def find_colonization_cost(self, origin_star, target_star):
        col_cost = distance(origin_star.coordinates, target_star.coordinates)
        return col_cost

    def update(self, surface, adjusted_mouse_pos):
        offset = self.star_map.offset
        font = self.star_map.font_object
        mouse_pos = pygame.mouse.get_pos()

        # COLONIZATION COST OVERLAY DISPLAY - Colonization Cost = distance

        # Finding focus
        self.focus = None

        for polity in self.star_map.polity_list: # Look if any polity is selected - if so, put the polity capital into focus
            if polity.is_selected == True:
               self.focus = polity.owned_stars[0]

        if self.focus == None: # If there were no selected polities (because an uninhabited star is selected), see which star is selected
            self.focus = self.map_action_menu.selected_star

        # Display focus
        star_pos = self.focus.coordinates
        screen_pos = [None,None]
        screen_pos[0] = star_pos[0] + offset[0]
        screen_pos[1] = star_pos[1] + offset[1]
        pygame.draw.circle(surface, (255, 0, 0), screen_pos, 50, 10)

        # Find colonization costs from focus star
        max_col_cost = 0
        for star in self.star_map.stars:
            col_cost = self.find_colonization_cost(self.focus, star)
            if col_cost > max_col_cost:
                max_col_cost = col_cost

        # Draw stars on the map
        for star in self.star_map.stars:

            # Displaying non_focus stars according to their colonization cost
            if self.focus != star:
                star_is_colonized = False
                if star.owner != None:
                    star_is_colonized = True
            
                if star_is_colonized == False:
                    screen_pos = [None, None] # Find coordinates for the star
                    screen_pos[0] = star.coordinates[0] + offset[0]
                    screen_pos[1] = star.coordinates[1] + offset[1]

                    col_cost = self.find_colonization_cost(self.focus, star) # Find the color of the cost - the higher it is, the darker the color
                    ratio = col_cost / max_col_cost
                    blue_color_strength = int(255 - (255 * ratio))
                    red_color_strength = 0

                    if self.focus.owner != None:
                        if self.focus.owner.money > col_cost:
                            blue_color_strength = 0
                            ratio = col_cost / self.focus.owner.money
                            red_color_strength = int(255 - (255 * ratio))

                    pygame.draw.circle(surface, (red_color_strength, 0, blue_color_strength), screen_pos, 50, 0)

            star.draw(surface, offset, font)

        # Draws the tooltip if required
        if self.map_action_menu.is_active == False:
            for star in self.star_map.stars:
                if distance(adjusted_mouse_pos, star.coordinates) < 20:
                    self.map_tooltip.clear_lines()

                    # First line of the tooltip - "[name] (Inhabited/Uninhabited)"
                    star_name_str = str(star.name)
                    if star.owner != None:
                        colonization_status_str = "(Inhabited)"
                    else:
                        colonization_status_str = "(Uninhabited)"
                    line = star_name_str + " " + colonization_status_str

                    self.map_tooltip.add_line(line)

                    # Second line of the tooltip - appears only if the star is uninhabited - Colonization Cost: [amount]
                    if star.owner == None:
                        fiel_name_str = "Colonization Cost:"
                        colonization_cost_str = str(int(self.find_colonization_cost(self.focus, star)))
                        line = fiel_name_str + " " + colonization_cost_str

                        self.map_tooltip.add_line(line)

                    self.map_tooltip.draw_tooltip(surface, mouse_pos, font)

        # Lets the action menu update and draw if it is active
        if self.map_action_menu.is_active == True:
            self.map_action_menu.update(adjusted_mouse_pos, offset)
            self.map_action_menu.draw(surface, offset, font)

        self.mouse_click(adjusted_mouse_pos)
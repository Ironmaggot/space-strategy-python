import pygame, random
from star_map import StarMap
from polity import Polity
from population import Population
from species_screen import SpeciesScreen

#Definitions for the application
screen_width = 1920
screen_heigth = 1080
resolution = (screen_width, screen_heigth)

pygame.init()

DISPLAYSURF = pygame.display.set_mode((screen_width, screen_heigth), pygame.FULLSCREEN)
DISPLAYSURF = pygame.display.set_caption("game")
DISPLAYSURF = pygame.display.get_surface()

background = pygame.Surface(DISPLAYSURF.get_size())

font = pygame.font.SysFont("Arial Black",16)

clock = pygame.time.Clock()

#---Game definitions initiation---

gameworld_size_X = 1000
gameworld_size_Y = 1000
max_planets = 3
amount_of_stars = 100
req_pop_points = 1600
    
#---Main Script---
runProgram = True
paused = True
tick = False
screen = "species_list"
time_passed = 0
star_map = StarMap(gameworld_size_X, gameworld_size_Y, amount_of_stars, resolution, font, max_planets)
species_screen = SpeciesScreen(font, resolution)
species_screen.species_list = star_map.specie_list

star_map.req_pop_points = req_pop_points

while runProgram == True:

    if paused == False:
        time_passed = time_passed + clock.tick()
        if time_passed > 1:
            time_passed = 0
            tick = True

    background.fill((0, 0, 0))
    DISPLAYSURF.blit(background, (0, 0))

    if screen == "star_map":
        star_map.update(DISPLAYSURF, tick)
        tick = False

    if screen == "species_list":
        species_screen.update(DISPLAYSURF)

    pygame.display.update()

    for event in pygame.event.get() :
        if event.type == pygame.QUIT:
            pygame.quit()
            runProgram = False
        if event.type == pygame.KEYDOWN :
            if event.key == pygame.K_ESCAPE :
                pygame.quit()
                runProgram = False
            elif event.key == pygame.K_SPACE :
                if paused == True:
                    paused = False
                else:
                    paused = True
            elif event.key == pygame.K_F1 :
                screen = "star_map"
            elif event.key == pygame.K_F2 :
                screen = "species_list"
        if event.type == pygame.MOUSEBUTTONDOWN:
            star_map.click_event = True
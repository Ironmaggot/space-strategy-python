import pygame
from button import button

class action_menu(object):
    """When something is selected, this class is probably responsible for displaying and handling whatever is selected"""

    def __init__(self, parent_overlay):
        
        self.coordinates = (0, 0)
        self.radius = 200
        self.is_active = False
        self.selected_star = None
        self.parent_overlay = parent_overlay

        # Making the colonize button
        self.colonization_button = button()
        self.colonization_button.coordinates_from_parent = [0, int(self.radius / -2)]
        self.colonization_button.color = (0, 200, 0)
        self.colonization_button.radius = 20
        self.colonization_button.parent = self

        # Visual assets
        self.menu_background = pygame.Surface((self.radius * 2, self.radius * 2))
        self.menu_background.set_colorkey((0, 0, 0))
        self.menu_background_rect = pygame.draw.circle(self.menu_background, (100, 100, 100), (self.radius, self.radius), self.radius, 0)
        self.menu_background.set_alpha(200)

    def mouse_click(self, offset):
        mouse_pos = pygame.mouse.get_pos()
        adjusted_coordinates_X = mouse_pos[0] - offset[0]
        adjusted_coordinates_Y = mouse_pos[1] - offset[1]
        adjusted_coordinates = (adjusted_coordinates_X, adjusted_coordinates_Y)

        if self.colonization_button.mouse_click(adjusted_coordinates) == True: # See if colonization button was clicked
            player = self.parent_overlay.get_player_polity()
            player_capital = player.owned_stars[0]
            
            if self.selected_star.owner == None: # Colonize the selected star if it is uninhabited
                self.parent_overlay.colonize(player_capital, self.selected_star)
            return True
        else:
            return False

    def draw(self, surface, offset, font):
        adjusted_coordinates_X = self.coordinates[0] + offset[0]
        adjusted_coordinates_Y = self.coordinates[1] + offset[1]
        adjusted_coordinates = (adjusted_coordinates_X, adjusted_coordinates_Y)

        self.menu_background_rect.center = (adjusted_coordinates)
        surface.blit(self.menu_background, self.menu_background_rect)

        pygame.draw.circle(surface, (200, 200, 200), adjusted_coordinates, self.radius, 3)

        self.selected_star.draw(surface, offset, font)

        #Draw a button, on the action_menu
        self.colonization_button.draw(surface, adjusted_coordinates, font)

    def update(self, adjusted_mouse_pos, offset):
        self.colonization_button.update(adjusted_mouse_pos)
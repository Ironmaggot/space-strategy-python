import random
from misc_functions import choose_random_star_name

class Species(object):
    """This class handles the race mechanics"""

    def __init__(self):
        self.name = choose_random_star_name()
        self.time_to_reproduce = random.randint(1, 3)
        self.preferred_gravity = random.randint(1, 3)
import pygame

class tooltip(object):
    """A class which displays tooltips"""

    def __init__(self):
        self.text = []
        self.base_line_Y_offset = 10
        self.base_line_Y_offset_step = 20

    def add_line(self, line):
        # A method which adds lines to the tooltip to be displayed
        self.text.append(str(line))

    def clear_lines(self):
        self.text = []

    def draw_tooltip(self, surface, mouse_pos, font_object):
        line_Y_offset = self.base_line_Y_offset
        max_line_width = 0

        for line in self.text:
            line_width = font_object.size(line)[0] 
            if max_line_width < line_width:
                max_line_width = line_width

        tooltip_width = 20 + max_line_width
        tooltip_height = (len(self.text) * self.base_line_Y_offset_step) + 20

        pygame.draw.rect(surface, (120, 120, 120), (mouse_pos, (tooltip_width, tooltip_height)), 0)
        pygame.draw.rect(surface, (200, 200, 200), (mouse_pos, (tooltip_width, tooltip_height)), 1)

        for line in self.text:
            tooltip_text = font_object.render(line, 0, (200, 200, 200))
            entry_coords = ((mouse_pos[0] + 10), (mouse_pos[1] + line_Y_offset))
            surface.blit(tooltip_text, entry_coords)
            line_Y_offset = line_Y_offset + self.base_line_Y_offset_step
import pygame

class SpeciesScreen(object):
    """This screen displays a list of species and information about them"""

    def __init__(self, font_object, resolution):
        self.species_list = []
        self.font_object = font_object
        self.resolution = resolution

    def update(self, surface):
        pygame.draw.rect(surface, (150, 150, 150), (10, 10, (self.resolution[0] - 20), 50))

        #-Display screen title
        screen_title_width = self.font_object.size("Species Screen")
        screen_title_X = int((self.resolution[0] / 2) - (screen_title_width[0] / 2))
        screen_title_coords = (screen_title_X, 20)
        screen_title_surf = self.font_object.render("Species Screen", 0, (255, 255, 255))
        surface.blit(screen_title_surf, screen_title_coords)

        pygame.draw.rect(surface, (200, 200, 200), (10, 70, (self.resolution[0] - 20), 50))

        displayed_list = self.species_list
        list_Y = 130
        list_height = 80
        list_step = 90
        for element in displayed_list:
            pygame.draw.rect(surface, (100, 100, 100), (10, list_Y, (self.resolution[0] - 70), list_height))
            element_text = element.name + " / " + str(element.time_to_reproduce)
            element_text_size = self.font_object.size(element_text)
            element_text_X = int((self.resolution[0] / 2) - (element_text_size[0] / 2))
            element_text_Y = int((list_Y + (list_height / 2)) - (element_text_size[1] / 2))
            element_text_coords = (element_text_X, element_text_Y)
            element_text_surf = self.font_object.render(element_text, 0, (255, 255, 255))
            surface.blit(element_text_surf, element_text_coords)
            list_Y = list_Y + list_step

        pygame.draw.rect(surface, (100, 100, 100), ((self.resolution[0] - 50), 130, 40, (self.resolution[1] - 140)))
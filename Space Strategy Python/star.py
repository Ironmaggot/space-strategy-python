import pygame, random
from misc_functions import distance

class Star(object):
    """This class handles the stars ingame"""

    def __init__(self, coordinates, i, name):
        self.name = name
        self.index = i
        self.coordinates = coordinates
        self.owner = None
        self.planets = []
        self.highlighted = False

    def draw(self, surface, offset, font):  
        owner_color = (255, 255, 255)      
        final_coordinates_X = self.coordinates[0] + offset[0]
        final_coordinates_Y = self.coordinates[1] + offset[1]
        final_coordinates = (final_coordinates_X, final_coordinates_Y)

        if self.owner != None: # Draw colored circle around owned stars
            owner_color = self.owner.color
            pygame.draw.circle(surface, owner_color, final_coordinates, 35, 0)

            if self.owner.is_selected == True:
                pygame.draw.circle(surface, (255, 255, 255), final_coordinates, 40, 5)
            
        pygame.draw.circle(surface, (255, 255, 255), final_coordinates, 3, 0)

        #Draw the name of the star
        if self.name != None:
            displayed_name = self.name
        else:
            displayed_name = self.index

        name_surf_size = font.size(str(displayed_name))
        name_surf_coords_X = self.coordinates[0] - (name_surf_size[0] / 2)
        name_surf = font.render(str(displayed_name), 0, (200, 200, 200))
        name_surf_coords = ((name_surf_coords_X + offset[0]), ((self.coordinates[1] + 20) + offset[1]))
        surface.blit(name_surf, name_surf_coords)

        # If mouse is hovered over a star(that is not selected) draw a tiny circle around the central dot
        if self.highlighted == True:
            pygame.draw.circle(surface, (255, 255, 255), final_coordinates, 7, 1)

        return surface

    def assign_owner(self, faction):
        self.owner = faction
        self.owner.owned_stars.append(self)

    def collect_tax(self, tax_rate):
        tax_collected = 0

        for planet in self.planets:
            population_size = len(planet.pop_list)
            tax_collected = population_size * tax_rate

        return tax_collected

    def update(self, offset):
        # Tasks of the function: [+] See if the star is hovered upon by mouse
        self.highlighted = False
        mouse_pos = pygame.mouse.get_pos()
        adjusted_mouse_pos_X = mouse_pos[0] - offset[0]
        adjusted_mouse_pos_Y = mouse_pos[1] - offset[1]
        adjusted_mouse_pos = [adjusted_mouse_pos_X, adjusted_mouse_pos_Y]

        if distance(adjusted_mouse_pos, self.coordinates) < 20:
            self.highlighted = True

class Planet(object):
    """This class handles the planets ingame"""

    def __init__(self):
        self.size = None # Base habitability constant on which all desirability calclulations are based on
        self.gravity = None
        self.index = None
        self.owner = None
        self.pop_list = []

    def find_base_desirability(self, pop):
        if pop.specie.preferred_gravity < self.gravity: #if pop prefers weaker gravity, use harsher malus
            gravity_modifier = (self.gravity - pop.specie.preferred_gravity) * 0.4 #Gravity malus -40% per relative difference
        elif pop.specie.preferred_gravity > self.gravity: #if pop prefers stronger gravity, use lighter malus
            gravity_modifier = (pop.specie.preferred_gravity - self.gravity) * 0.2 # -20% per relative difference
        else:
            gravity_modifier = 0

        base_desirability = (self.size * (1 - gravity_modifier))

        return base_desirability

    def find_desirability(self, pop):

        base_desirability = self.find_base_desirability(pop)

        desirability = base_desirability - len(self.pop_list)

        return desirability
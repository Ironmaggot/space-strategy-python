import pygame
from tooltip import tooltip
from misc_functions import distance

class button(object):
    """description of class"""

    def __init__(self):
        self.coordinates_from_parent = [0, 0] # The menu or screen or wherever this button is, is its parent
        self.highlighted = False
        self.color = (0, 0, 0)
        self.radius = 0
        self.parent = None

        self.tool_tip = tooltip()

        self.tool_tip_lines = []
        self.tool_tip_lines.append("COLONIZE")
        self.tool_tip_lines.append("")
        self.tool_tip_lines.append("Will establish a colony on this star")

    def mouse_click(self, adjusted_mouse_pos):
        own_coordinate_X = self.parent.coordinates[0] + self.coordinates_from_parent[0]
        own_coordinate_Y = self.parent.coordinates[1] + self.coordinates_from_parent[1]
        own_coordinates = [own_coordinate_X, own_coordinate_Y]

        if distance(adjusted_mouse_pos, own_coordinates) < self.radius:
            return True
        else:
            return False

    def update(self, adjusted_mouse_pos):
        self.highlighted = False

        # Check if mouse is hovering over this button
        adjusted_coordinates_X = (self.parent.coordinates[0] + self.coordinates_from_parent[0])
        adjusted_coordinates_Y = (self.parent.coordinates[1] + self.coordinates_from_parent[1])
        adjusted_coordinates = [adjusted_coordinates_X, adjusted_coordinates_Y]

        if distance(adjusted_mouse_pos, adjusted_coordinates) < self.radius:
            self.highlighted = True

    def draw(self, surface, parent_coords, font):
        adjusted_coords_X = parent_coords[0] + self.coordinates_from_parent[0]
        adjusted_coords_Y = parent_coords[1] + self.coordinates_from_parent[1]
        adjusted_coords = [adjusted_coords_X, adjusted_coords_Y]

        mouse_pos = pygame.mouse.get_pos()

        if self.parent.selected_star.owner == None:
            button_color = self.color
        else:
            button_color = (100, 100, 100)

        if self.highlighted == False:
            pygame.draw.circle(surface, button_color, adjusted_coords, self.radius, 0)
        else:
            pygame.draw.circle(surface, button_color, adjusted_coords, (self.radius - 3), 0)
            pygame.draw.circle(surface, button_color, adjusted_coords, (self.radius + 2), 2)

            # ADD APPROPRIATE LINES TO THE TOOLTIP AND DRAW IT
            if self.parent.selected_star.owner == None:
                self.tool_tip.clear_lines()
                for line in self.tool_tip_lines:
                    self.tool_tip.add_line(line)

                # Adds the cost of colonizing to the tooltip
                field_name_str = "Cost:"
                target_star = self.parent.selected_star

                # Finding the player capital star
                origin_star = target_star
                for star in self.parent.parent_overlay.star_map.stars:
                    if star.owner != None:
                        if star.owner.player == True:
                            origin_star = star.owner.owned_stars[0]
                            break

                cost_str = str(int(self.parent.parent_overlay.find_colonization_cost(origin_star, target_star)))

                line = field_name_str + " " + cost_str
                self.tool_tip.add_line(line)
            
            else:
                self.tool_tip.clear_lines()
                self.tool_tip.add_line("COLONIZE")
                self.tool_tip.add_line("Cannot colonize - star is already inhabited")

            self.tool_tip.draw_tooltip(surface, mouse_pos, font)